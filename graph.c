/*****************************************************
*
*
*	Author: Endri Kastrati
*
*	Function definition for graph operations.
*	Written for assignment2 of COMP20007.
*
*
******************************************************/


/*Including the standard input-output library.*/
#include <stdio.h>

/*Including the standard general utilities library.*/
#include <stdlib.h>

/*Including the standard assertion library.*/
#include <assert.h>

/*Including the graph library.*/
#include "graph.h"



/*This function creates a new Graph data structure
and returns a pointer to that struct.*/
Graph *graph_new(int number_of_vertices)
{
    /*Variable declarations and initialisations
    and assertions.*/
	int i,init_edges_size=1;
	Graph *g=NULL;
    assert(number_of_vertices>0);


    /*Allocate memory for the Graph data structure
    and all of it's components.Once done,
    return pointer to that struct.*/

	g=(Graph *)malloc(sizeof(*g));
	assert(g!=NULL);
	g->number_of_vertices=number_of_vertices;
	g->vertices=(Vertex *)malloc(number_of_vertices*sizeof(Vertex));
	assert(g->vertices!=NULL);

    for (i=0;i<number_of_vertices;i++)
    {
        g->vertices[i].edges=(Edge *)malloc(init_edges_size*sizeof(Edge));
        assert(g->vertices[i].edges!=NULL);
        g->vertices[i].num_edges=0;
        g->vertices[i].max_num_edges=init_edges_size;
    }

    return g;
}



/*This function adds an edge from a vertex v to a vertex u
that belong to a graph data structure.*/
void graph_add_edge(Graph *g, Label v, Label u, void *data)
{
    /*Variable declarations and initialisations.*/
    int double_size;
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);
    assert(u >= 0 && u < g->number_of_vertices);

    /*Check if vertex v has enough space for the new edge,
    if not reallocate more memory.*/

    if (g->vertices[v].num_edges+1==g->vertices[v].max_num_edges)
    {
        double_size=2*g->vertices[v].max_num_edges;
        g->vertices[v].edges=(Edge *)realloc(g->vertices[v].edges,double_size*sizeof(Edge));
        assert(g->vertices[v].edges!=NULL);
        g->vertices[v].max_num_edges=double_size;
    }

    /*Add edge,edge weight and increment the last index
    of the edges array.*/

    g->vertices[v].edges[g->vertices[v].num_edges].data=data;
    g->vertices[v].edges[g->vertices[v].num_edges].u=u;
    g->vertices[v].num_edges+=1;
    return;
}



/*This function deletes an edge from vertex v
to vertex u that belong to a graph data structure.*/
void graph_del_edge(Graph *g, Label v, Label u)
{
    /*Variable declarations and assertions.*/
    int i;
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);
    assert(u >= 0 && u < g->number_of_vertices);

    /*Loop through all edges of vertex v.
    If vertex v has an edge to u and that
    edge hasn't been deleted, then set the
    data component of that edge to indicate
    deletion.*/

    for (i=0;i<g->vertices[v].num_edges;i++)
    {
        if (g->vertices[v].edges[i].u==u && g->vertices[v].edges[i].data!=(void *)(-1))
        {
            g->vertices[v].edges[i].data=(void *)(-1);
        }
    }

    return;
}



/*This function returns an pointer to an array of edges
that belong to a vertex v.*/
Edge *graph_get_edge_array(Graph *g, Label v, int *num_edges)
{
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);
    *num_edges = g->vertices[v].num_edges;
    return g->vertices[v].edges;
}




/*This function checks if a Graph data structure
has an edge (v,u).*/
int graph_has_edge(Graph *g, Label v, Label u)
{
    /*Variable declarations and assertions.*/
    int i;
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);
    assert(u >= 0 && u < g->number_of_vertices);

    /*Loop through all edges of vertex v.
    If vertex v has an edge to u and that edge
    hasn't been deleted return one.Otherwise
    return zero.*/

    for (i=0;i<g->vertices[v].num_edges;i++)
    {
        if (g->vertices[v].edges[i].u==u && g->vertices[v].edges[i].data!=(void *)(-1))
        {
            return 1;
        }
    }

    return 0;
}



/*This function adds some data to the specified vertex.*/
void graph_set_vertex_data(Graph *g, Label v, void *data)
{
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);
    g->vertices[v].data = data;
    return;
}



/*This higher order function performs depth frist search in
a graph data structure.*/
void graph_dfs(Graph *g,Label v,void (explore)(Graph *g,Label v))
{
    /*Assertions and variable declarations.*/
	int i;
    assert(g);
    assert(explore);
    assert(v >= 0 && v < g->number_of_vertices);    

    /*Run explore on vertex v and all other 
    vertices that exist in the graph.*/

    explore(g,v);

    for (i=0;i<g->number_of_vertices;i++)
    {
    	explore(g,i);
    }

    return;
}



/*This function deallocates memory
for a Graph data structure and all of
it's components.*/
void graph_destroy(Graph *g)
{
    int i;

    for (i=0;i<g->number_of_vertices;i++)
    {
        free(g->vertices[i].edges);
    }

    free(g->vertices);
    free(g);
    return;
}
