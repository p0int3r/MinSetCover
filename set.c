/*****************************************************
*
*
*	Author: Endri Kastrati
*
*	Function defintion for Set/s operations.
*	Written for assignment2 of COMP20007.
*
*
******************************************************/


/*Including the standard input-output library.*/
#include <stdio.h>

/*Including the standard general utilities library.*/
#include <stdlib.h>

/*Including the standard general assertion library.*/
#include <assert.h>

/*Including the set library.*/
#include "set.h"



/*This function creates a new Sets data
structure and returns a pointer to that
new structure.*/
Sets *create_sets_array(uint number_of_sets)
{
	/*Variable declarations and assertions.*/
	assert(number_of_sets>=0);
	Sets *sets;

	/*Memory allocation for the new Set data
	structure and all of it's components.*/

	sets=(Sets *)malloc(sizeof(*sets));
	assert(sets!=NULL);
	sets->S=(Set **)malloc(number_of_sets*sizeof(*sets));
	assert(sets->S!=NULL);
	sets->map=(uint *)malloc(number_of_sets*sizeof(uint ));
	assert(sets->map!=NULL);

	/*Set size to the number of sets
	given as parameter and return
	pointer to new Sets struct.*/

	sets->size=number_of_sets;
	return sets;
}



/*This function creates a new,empty Set data structure
and returns a pointer to that struct.*/
Set *create_set()
{
	/*Variable declarations and initialisations.*/
	int initial_array_size=1;
	Set *set;

	/*Allocate memory for the new Set data structure
	and it's components.*/

	set=(Set *)malloc(sizeof(*set));
	assert(set!=NULL);
	set->A=(uint *)malloc(initial_array_size*sizeof(uint ));
	assert(set->A!=NULL);

	/*Set index of array component to zero,
	initial size to one and return pointer
	to new Set struct.*/

	set->n=0;
	set->size=initial_array_size;
	return set;
}



/*This function inserts a new item into
a Set data structure.*/
void insert_into_set(Set *s,uint item)
{

	/*Variable declarations and assertions.*/
	int double_size;
	assert(s!=NULL);
	assert(item>=0);

	/*If Array is full,allocate more memory
	before inserting the item.*/

	if (s->n+1==s->size)
	{
		double_size=2*s->size;
		s->A=(uint *)realloc(s->A,double_size*sizeof(uint ));
		assert(s->A!=NULL);
		s->size=double_size;
	}

	/*Insert the item into the set
	and increment the last index by one*/

	s->A[s->n]=item;
	s->n++;
	return;
}



/*A private function, that implements counting sort,extracted
from the wikipedia page:
 http://rosettacode.org/wiki/Sorting_algorithms/Counting_sort#C .*/
static void counting_sort_mm(uint *array,uint n,uint min,uint max)
{
	uint i,j,z;
	uint range,*count;
	range=max-min+1;
	count=(uint *)malloc(range*sizeof(*array));
	assert(count!=NULL);

	for (i=0;i<range;i++)
	{
		count[i]=0;
	}

	for (i=0;i<n;i++)
	{
		count[array[i]-min]++;
	}

	for (i=min,z=0;i<=max;i++)
	{
		for (j=0;j<count[i-min];j++)
		{
			array[z++]=i;
		}
	}

	free(count);
}



/*This function joins two Set data structures into
a new one,deallocates memory for the first given
Set parameter and returns pointer to new Set struct.*/
Set *join_sets(Set *s1,Set *s2,uint subset)
{

	/*Variable declarations,initialisations and assertions.*/
	assert(s1!=NULL);
	assert(s2!=NULL);
	Set *new_set;
	uint i=0,s1_size=s1->n,s2_size=s2->n;

	/*Create a new,empty Set data structure,
	and insert into it the entire first Set parameter.*/

	new_set=create_set();

	for (i=0;i<s1_size;i++)
	{
		insert_into_set(new_set,s1->A[i]);
	}

	/*Once first Set parameter has been inserted into the new set,
	insert from second Set parameter only items
	that do not exist in the new set.*/

	for (i=0;i<s2_size;i++)
	{

		if (i!=0 && s2->A[i]>new_set->A[new_set->n-1])
		{
			insert_into_set(new_set,s2->A[i]);
			continue;
		}
		else if (i!=0 && s2->A[i]==new_set->A[new_set->n-1])
		{
			continue;
		}
		else if (search_set(new_set,s2->A[i])==0)
		{
			insert_into_set(new_set,s2->A[i]);
		}
	}

	/*Deallocate memory for the first Set parameter,
	sort new Set using counting sort and return
	pointer to new Set struct.*/

	destroy_set(s1);
	counting_sort_mm(new_set->A,new_set->n,0,subset);
	return new_set;
}



/*A private function that implemenets a recursive binary search.
Skeleton taken from Alistair's moffat book:
'''Programming,Problem solving and Abstraction with C'''.*/
static int binary_search(uint *A,uint item,int low,int high)
{
	int mid,outcome;

	if (low>=high)
	{
		return NOT_FOUND;
	}

	mid=(low+high)/2;
	outcome=item-A[mid];
	
	if (outcome<0)
	{
		return binary_search(A,item,low,mid);
	}
	else if (outcome>0)
	{
		return binary_search(A,item,mid+1,high);
	}
	else
	{
		return FOUND;
	}
}


/*This function searches a sorted set for a 
specific item via recursive binary search.*/
int search_set(Set *s,uint item)
{
	int i;
	assert(s!=NULL);
	assert(item>=0);
	return binary_search(s->A,item,0,s->n);
}



/*This function returns the set
difference between two Set data structures.*/
int substract_sets(Set *s1,Set *s2)
{
	/*Variable declarations,initialisations and assertions.*/
	assert(s1!=NULL && s2!=NULL);
	uint count=0;
	int i;

	/*Iterate through one of the sets and
	lookup the ith item into the other set,
	if it does not exist increment count by one.*/

	for (i=0;i<s1->n;i++)
	{
		if (search_set(s2,s1->A[i])==NOT_FOUND)
		{
			count++;
		}
	}

	return count;
}



/*This function deallocates memory for a Set
data structure.*/
void destroy_set(Set *s)
{
	assert(s!=NULL);
	free(s->A);
	free(s);
	return;
}



/*This function deallocates all memory
for a Sets data structure.*/
void destroy_sets_array(Sets *sets)
{
	int i;
	assert(sets!=NULL);

	for (i=0;i<sets->size;i++)
	{
		destroy_set(sets->S[i]);
	}

	free(sets->map);
	free(sets->S);
	free(sets);
	return;
}

