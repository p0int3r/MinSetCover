/*****************************************************
*
*
*	Author: Endri Kastrati
*
*	Function prototyping for assignment2 main program.
*	Written for assignment2 of COMP20007.
*
*
******************************************************/

/*Including the graph library.*/
#include "graph.h"

/*Including the set library.*/
#include "set.h"

/*Including the heap library.*/
#include "heap.h"



/*A function like Macro that dereferences void pointers to the specified type.*/
#define VOID_DEREF(type,pointer) ((type *)pointer)


/*Constant definition of boolean values.*/
#define SUCCESS 	1
#define FAIL 		0



/*A union data structure to enable an object to be of either
float,long or long* at any given time.The reason for creating 
this union type is to provide smooth interaction with the Graph and Heap data structure.*/
typedef union
{
	float 	to_float; 
	long 	to_long;
	long 	*to_lptr;
} custom_type;




/*This data structure is used to assist the score evaluation of the sets during the greedy approach.
The score components represents the uncovered elements,the length component the size of the set,
and the id the set label.*/
typedef struct
{
	int 	score;
	int 	length;
	int		id;
} Setdata;



/*The unsused data structure stores the sets that did not have the best score.It has components
and array of Setdata structs,current size and the total allocation size.*/
typedef struct
{
	Setdata 	*D;
	uint 		n;
	uint 		size;
} Unused;




/*All functions operate on the assertion that the given data structures are not NULL and that
the variables of uint type are greater or equal to zero, if one of these assertions fails,the
execution terminates immediately.*/



/****************************************************************************************************************
The check_vertex_range() function, checks if the read vertex is within the specified boundaries.

	Parameters:
		char *filename,			A pointer to a string,which is the name of the file.
		uint vertex,			An unsigned integer that represents the vertex.
		uint lower,				An unsigned integer represeting the lower boundary of the vertex.
		uint upper,				An unsigned integer representing the upper boundary of the vertex.

	Return values:
		If the vertex is within the given boundaries an integer value 1 (positive) is returned.
		Otherwise a message alerting the user of invalid format is printed and program exeuction
		terminates immediately.
****************************************************************************************************************/
int 	check_vertex_range(char *filename,uint vertex,uint line_number,uint lower,uint upper);





/****************************************************************************************************************
The *read_connected_graph() function reads data from a file,creates,populates adn returns a Graph data structure.

	Parameters:
		char *filename,		A pointer to a string,which refers to the file name.
		uint *subsetA,		A pointer to an unsigned integer that will hold the range of the first vertices subset.
		uint *subsetB,		A pointer to an unsigned integer that will hold the range of the second vertices subset.

	Return values:
		If the graph is connected and there are no formatting errors,returns a pointer to a Graph data structure.
		Otherwise,it prints an appropriate message and terminates execution.
****************************************************************************************************************/
Graph 	*read_connected_graph(char *filename,uint *subsetA,uint *subsetB);





/****************************************************************************************************************
The *run_dijkstra() function,Runs Dijkstra's algorithm on a source vertex and returns a Set data structure
containing all reachable vertices up to the specified radius.

	Parameters:
		Graph *g,		A pointer to a Graph data structure.
		uint  source,	An unsigned integer that represents the source vertex.
		float radius,	A float number representing the maximum radius which the source vertex can cover.
		uint  subset,	An unsigned integer,which represents the type of vertices we want in the set.

	Return values:
		if execution is successfuly,it returns a pointer to a Set data structure containing all vertices that
		are reachable from source vertex within the given radius and that belong to the specified subset.
****************************************************************************************************************/
Set 	*run_dijkstra(Graph *g,uint source,float radius,uint subset);





/****************************************************************************************************************
The *greedy_set_cover() function,uses a greedy technique to calculate the minimum set of vertices that belong to
a subset A such that they cover all reachable vertices that belong to a subset B.

	Parameters:
		Sets *sets,		A pointer to a Sets data structure that contains the sets for each vertex that belongs to
							a subset A.
		Set *X,			A pointer to a Set data structure that contains the sum of all reachable vertices that
							belong a subset B.
		uint subset,	An unsigned integer to a subset we want to cover.

	Return values:
		If successful,it returns a pointer to a Set data structure that contains the minimum set of vertices
		that belong to a subset A and that they cover all reachable vertices that belong to a subset B.
****************************************************************************************************************/
Set 	*greedy_set_cover(Sets *sets,Set *X,uint subset);



/****************************************************************************************************************
The edgesCompare() function, compares two Edge data structures by returning the difference of their
data components (edge weight).

	Parameters:
		const void *p1, 	First Edge data structure.
		const void *p2,		Second Edge data structure.

	Return values:
		Returns an integer, negative or positive or zero.If negative weight of first edge is less
		than the weight of second,if positive greater than second and if zero the weights are equal.
****************************************************************************************************************/
int 	edgesCompare(const void *p1,const void *p2);



/****************************************************************************************************************
The unused_append() function, adds a new Setdata into the Unused data structure.

	Parameters:
		Unused *u,		A pointer to an Unused struct.
		Setdata sd,		a Setdata struct.

	Return values:
		returns nothing.
****************************************************************************************************************/
void 	unused_append(Unused *u,Setdata sd);




/****************************************************************************************************************
The print_results() function, prints the results.

	Parameters:
		Set *r, 	A pointer to a Set data structure.

	Return values:
		Returns nothing,prints to the standard output the result set,a vertex per line.
****************************************************************************************************************/
void 	print_results(Set *r,uint subset);




/****************************************************************************************************************
The unused_free() function, deallocates all memory for a Unused data structure.

	Parameters:
		Unused *u,		A pointer to an Unused data structure.

	Return values:
		returns nothing.
****************************************************************************************************************/
void 	unused_free(Unused *u);




/****************************************************************************************************************
The *create_unused() function,creates a new,empty Unused data structure.

	Parameters:
		None

	Return values:
		returns a pointer to a new Unused data structure.
****************************************************************************************************************/
Unused 	*create_unused();
