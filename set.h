/*****************************************************
*
*
*	Author: Endri Kastrati
*
*	Set data type and function prototyping.
*	Written for assignment2 of COMP20007.
*
*
******************************************************/

/*Constant definitions of Boolean values.*/
#define FOUND 		1
#define NOT_FOUND 	0


/*A shortcut representation of an unsigned integer.*/
typedef unsigned int uint;



/*A Set data structure,comprises of an
array of unsigned integers,the current size,
total allocated size.*/
typedef struct
{
	uint 	*A;
	uint 	n;
	uint 	size;
} Set;



/*The Sets data structure, comprises of an array
of pointers to Set structs,an array that maps some values
to each Set, and the total allocated size.*/
typedef struct
{
	Set 	**S;
	uint 	*map;
	uint 	size;
} Sets;




/*All functions operate on the assertion that the given data structures are not NULL and that
the variables of uint type are greater or equal to zero, if one of these assertions fails,the
execution terminates immediately.*/




/****************************************************************************************************************
The *create_set() function, creates a new,empty Set data structure.

	Parameters: 	None.

	Return values:
		returns a pointer to a Set data structure.
****************************************************************************************************************/
Set 	*create_set();



/****************************************************************************************************************
The destroy_set() function,deallocates all memory for a Set struct and all of it's components.
	
	Parameters:
		Set *s,		A pointer to a Set struct.

	Return values.
		returns nothing.
****************************************************************************************************************/
void 	destroy_set(Set *s);



/****************************************************************************************************************
The search_set() function,performs a binary search on a Set struct to locate an item.
	
	Parameters:
		Set *s,		A pointer to a Set data structure.
		uint item,	An unsigned integer which represents the item to be searched for.

	Return values:
		returns one if the item was found and zero if it wasn't.
****************************************************************************************************************/
int 	search_set(Set *s,uint item);



/****************************************************************************************************************
The destroy_sets_array(), deallocates memory for a Sets data structure and all of it's components.

	Parameters:
		Sets *sets,		A pointer to a Sets data structure.

	Return values:
		returns nothing.
****************************************************************************************************************/
void 	destroy_sets_array(Sets *sets);



/****************************************************************************************************************
The substract_sets() function,returns the difference of two Set data structures.

	Parameters:
		Set *s1,	A pointer to the first set data structure.
		Set *s2,	A pointer to the second set data structure.

	Return values:
		returns an integer that prepresents the total difference between the two sets.
****************************************************************************************************************/
int 	substract_sets(Set *s1,Set *s2);



/****************************************************************************************************************
The insert_into_set() function, inserts a new element into the specified Set data structure.

	Parameters:
		Set *s,		A pointer to a Set data structure.
		uint item,	An unsigned integer.

	Return values:
		Returns nothing.
****************************************************************************************************************/
void 	insert_into_set(Set *s,uint item);



/****************************************************************************************************************
The *create_sets_array() function, allocates memory for a new Sets data structure and all of it's components.

	Parameters:
		uint number_of_sets, 		An unsigned integer that specifies the size of the Sets struct.

	Return values:
		returns a pointer to a Sets data structure.
****************************************************************************************************************/
Sets 	*create_sets_array(uint number_of_sets);



/****************************************************************************************************************
The *join_sets() function, joins the unique elements of two Set data structures into a new Set in sorted order.
ATTENTION:deallocates memory for the first set parameter, used like this:
														X=join_sets(X,s1);

	Parameters:
		Set *s1, 		Pointer to first Set.
		Set *s2,		Pointer to second Set.
		uint subset, 	An unsigned integer representing the the total vertices that have been covered.
							Needed for counting sort.

	Return values:
		A pointer to a new Set data structure.
****************************************************************************************************************/
Set 	*join_sets(Set *s1,Set *s2,uint subset);
