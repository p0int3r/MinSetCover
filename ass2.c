/*****************************************************
*
*
*	Author: Endri Kastrati
*
*	Function definition for assignment2 main program.
*	Written for assignment2 of COMP20007.
*
*
******************************************************/

/*Including the standard input-output library.*/
#include <stdio.h>

/*Including the standard general utilities library.*/
#include <stdlib.h>

/*Including the general assertion library.*/
#include <assert.h>

/*Including the ass2 library.*/
#include "ass2.h"

/*Including the math library.*/
#include <math.h>



/*This function reads input from a file and creates a Graph, if the input
is incorrect it prints an appropriate message and terminates execution.*/
Graph *read_connected_graph(char *filename,uint *subsetA,uint *subsetB)
{

	/*Variable declarations and initializations.*/
	uint max_chars=30,line_number=1,read,invalid,total_edges=0;
	uint v1,v2;
	custom_type edge_weight;
	char line[max_chars];
	Graph *g;

	/*Start reading input from the file a line at a time until
	NULL is encountered.*/

	while (fgets(line,max_chars,stdin)!=NULL)
	{
		if (line_number==1)
		{
			/*Reading the first subset,if format incorrect print
			a message in stderr and terminate and strip from the stack
			all frames associated with this program.*/

			read=sscanf(line,"%d %d",subsetA,&invalid);

			if (read>1)
			{
				fprintf(stderr,"The format of first row is incorrect,multiple inputs.\n");
				exit(EXIT_FAILURE);

			}
		}

		if (line_number==2)
		{
			/*Read the second subset,if format incorrect print
			a message in stderr and strip from the stack
			all frames associated with this program.*/

			read=sscanf(line,"%d %d",subsetB,&invalid);

			if (read>1)
			{
				fprintf(stderr,"The format of the second row is incorrect,multiple inputs.\n");
				exit(EXIT_FAILURE);
			}

			/*Create a new Graph data structure with a total number of vertices
			equal to the sum of the first and the second subset that were read.*/

			g=graph_new(*subsetA+*subsetB);
		}

		if (line_number>=3)
		{
			/*Read a pair of vertices and it's edge weight, if format is incorrect
			print a message in stderr and terminate execution.*/

			read=sscanf(line,"%d %d %f",&v1,&v2,&edge_weight.to_float);
			
			if (read<3 || read>3)
			{
				fprintf(stderr,"line %d: format is incorrect.\n",line_number);
				exit(EXIT_FAILURE);
			}

			/*Check if the read pair is within the Graph boundaries,
			add edges into the graph and keep track of number of edges
			being added.*/

			check_vertex_range(filename,line_number,v1,0,*subsetA+*subsetB);
			check_vertex_range(filename,line_number,v2,0,*subsetA+*subsetB);
			graph_add_edge(g,v1,v2,edge_weight.to_lptr);
			graph_add_edge(g,v2,v1,edge_weight.to_lptr);
			total_edges++;
		}

		line_number++;	/*Increment line number by one.*/
	}

	/*If the total number of edges is less than the sum of the two subsets
	minus one, then graph is not connected,print a message to stderr and
	strip from the stack all frames associated with this program.*/

	if (total_edges<(*subsetA+*subsetB)-1)
	{
		fprintf(stderr,"Graph is not connected.\n");
		exit(EXIT_FAILURE);
	}

	return g;
}



/*This function compares the weights of two edges by returning
their difference.*/
int edgesCompare(const void *p1,const void *p2)
{
	/*Variable declarations,initializations and dereferencings.*/
	Edge *e1=(Edge *)p1,*e2=(Edge *)p2;
	custom_type w1,w2;
	w1.to_lptr=VOID_DEREF(long,e1->data);
	w2.to_lptr=VOID_DEREF(long,e2->data);

	return (int )(w1.to_float-w2.to_float);
}



/*This function runs dijkstra's algorithm from a source vertex 
that belongs to a subset A and returns a set of unique vertices that belong
to a subset B that are reachable within the specified radius.*/
Set *run_dijkstra(Graph *g,uint source,float radius,uint subset)
{
	/*Variable declarations.*/
	uint i,j,vertex_u,vertex_v;
	int u_elements;
	float new_distance,*distances;
	Heap *heap;
	Set *set;
	custom_type lvu;
	Edge *u_edges;

	/*Allocating memory for an array that will contain
	the initial distances for every vertex.Creating empty
	Heap and Set data structure.*/

	distances=(float *)malloc(g->number_of_vertices*sizeof(*distances));
	assert(distances!=NULL);
	heap=createHeap();
	set=create_set();

	/*Initialize all distances to INFINITY except
	the distance of the source vertex which is
	set to zero,and insert them into the heap.*/

	for (i=0;i<g->number_of_vertices;i++)
	{
		if (i==source)
		{
			distances[i]=0.0;
		}
		else
		{
			distances[i]=INFINITY;
		}

		insert(heap,i,distances[i]);
	}

	/*Run dijkstra's algorithm,start popping vertices
	from the heap until heap is empty.*/

	while (heap->n!=0)
	{
		/*Remove vertex u with minimum distance from the heap
		and sort it's edges.*/

		vertex_u=removeMin(heap);
		u_edges=graph_get_edge_array(g,vertex_u,&u_elements);
		qsort(u_edges,u_elements,sizeof(Edge ),edgesCompare);

		/*Iterate over all edges of vertex u, calculate
		the distance to each vertex v.*/

		for (i=0;i<g->vertices[vertex_u].num_edges;i++)
		{
			vertex_v=g->vertices[vertex_u].edges[i].u;
			lvu.to_lptr=VOID_DEREF(long,g->vertices[vertex_u].edges[i].data);
			new_distance=distances[vertex_u]+lvu.to_float;

			/*If distance exceeds radius,the free resources
			and return the set.*/

			if (new_distance>radius)
			{
				free(distances);
				destroyHeap(heap);
				return set;
			}

			/*If new distance is greater than the previous
			one then overwrite previous distance with
			new one and reheapify.*/

			if (distances[vertex_v]>new_distance)
			{
				distances[vertex_v]=new_distance;

				/*If vertex v belongs to the specified
				subset then add v into the set.*/

				if (vertex_v<subset)
				{
					if (search_set(set,vertex_v)==0)
					{
						insert_into_set(set,vertex_v);
					}
				}

				changeKey(heap,vertex_v,new_distance);
			}
		}
	}

	/*If execution did not terminate early,free resources
	and return set.*/

	free(distances);
	destroyHeap(heap);
	return set;
}



/*The greedy algorithm for an 1+logn approximation
to optimal solution for minimum set cover problem.*/
Set *greedy_set_cover(Sets  *sets,Set *X,uint subset)
{
	/*Variable declarations,initialisations
	and assertions.*/
	int i,j;
	int score,length,set_id;
	assert(sets!=NULL);
	Heap *heap;
	Set *union_set,*results,*substracted;
	Setdata best,temp;
	Unused *unused;

	/*Create a Heap and two Set 
	data structures.*/

	heap=createHeap();
	union_set=create_set();
	results=create_set();

	/*Insert sets into the heap using
	length of X minus the length of set i
	as a score.*/

	for (i=0;i<sets->size;i++)
	{
		insert(heap,i,X->n-sets->S[i]->n);
	}

	/*Iterate until all elements have been
	covered.*/

	while (union_set->n<X->n)
	{
		/*Initialise best score to empty
		and create a new,empty Unused 
		data structure.*/

		best.score=-1;
		best.length=-1;
		best.id=-1;
		unused=create_unused();

		/*Start popping elements from the
		heap until heap is empty.*/

		while (heap->n!=0)
		{
			/*Remove set with minimum score and
			assign it's score,length and id to
			temporary variables.If best is empty,
			initialise it with the temporary variables.*/

			score=peekKey(heap);
			length=sets->S[peek(heap)]->n;
			set_id=removeMin(heap);

			if (best.score==-1)
			{
				best.score=X->n-(substract_sets(sets->S[set_id],union_set));
				best.length=length;
				best.id=set_id;
				continue;
			}

			if (score>=best.score)
			{
				insert(heap,set_id,score);
				break;
			}

			score=X->n-(substract_sets(sets->S[set_id],union_set));

			if (score>=best.score)
			{
				temp.score=score;
				temp.length=length;
				temp.id=set_id;
				unused_append(unused,temp);
			}
			else
			{
				best.score=score;
				best.length=length;
				best.id=set_id;
				unused_append(unused,best);
			}
		}

		insert_into_set(results,sets->map[best.id]);
		union_set=join_sets(union_set,sets->S[best.id],subset);

		/*Sets that were not the best get put back into the heap
		for next time.*/

		for (i=unused->n-1;i>=0;i-=1)
		{
			insert(heap,unused->D[i].id,unused->D[i].score);
		}

		unused_free(unused);
	}

	/*Everything is done,tidy up and free resources,return a
	pointer to the results set data structure.*/

	destroy_set(union_set);
	destroyHeap(heap);
	return results;
}



/*This function checks if the read vertex is within the specified boundaries.*/
int check_vertex_range(char *filename,uint line_number,uint vertex,uint lower,uint upper)
{
	/*If vertex is within boundaries return SUCCESS macro,otherwise print a message
	in stderr and strip from the stack all frames associated with this program.*/

	if (vertex>=lower && vertex<upper)
	{
		return SUCCESS;				
	}
	else
	{
		fprintf(stderr,"File:%s\n",filename);
		fprintf(stderr,"line %d: Vertex %d is out of range.\n",line_number,vertex);
		exit(EXIT_FAILURE);
	}

}


/*A function to print the minimum set cover.*/
void print_results(Set *r,uint subset)
{
	int i;

	/*Print a vertex per line.*/

	for (i=0;i<r->n;i++)
	{
		printf("%d\n",r->A[i]-subset);
	}

	return;
}



/*This function creates a new,empty Unused data structure.*/
Unused *create_unused()
{
	/*Variable declarations,initialisations,
	assertions and memory allocations.*/
	Unused *u;
	uint initial_size=1;
	u=(Unused *)malloc(sizeof(*u));
	assert(u!=NULL);
	u->D=(Setdata *)malloc(initial_size*sizeof(Setdata ));
	assert(u->D!=NULL);
	u->n=0;
	u->size=initial_size;

	return u;	/*Return pointer to new Unused data structure.*/
}




/*This function adds items into an Unused data structure.*/
void unused_append(Unused *u,Setdata sd)
{
	/*Variable declarations and assertions.*/
	uint double_size;
	assert(u!=NULL);

	/*Check if data structure has enough space,
	if not allocate more memory.*/

	if (u->n+1==u->size)
	{
		double_size=2*u->size;
		u->D=(Setdata *)realloc(u->D,double_size*sizeof(Setdata ));
		assert(u->D!=NULL);
		u->size=double_size;
	}

	/*Insert item into the array,
	and increment the index.*/

	u->D[u->n]=sd;
	u->n++;
}



/*This function deallocates memory
for a Unused data structure.*/
void unused_free(Unused *u)
{
	assert(u!=NULL);
	free(u->D);
	free(u);
	return;
}
