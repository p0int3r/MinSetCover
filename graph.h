/*****************************************************
*
*
*	Author: Endri Kastrati
*
*	Graph data type and function prototyping.
*	Written for assignment2 of COMP20007.
*
*
******************************************************/


/*Defining a data type called Label,which will be used
to label the vertices of the graph using integers.*/
typedef int Label;



/*The Edge data structure,representing the 
abstract concept of edges.*/
typedef struct
{
	void 	*data;
	Label	u;
} Edge;



/*The Vertex data structure,representing the 
abstract concept of vertices.*/
typedef struct
{
	void 	*data;
	Label	label;
	Edge	*edges;
	int 	num_edges;
	int 	max_num_edges;
} Vertex;



/*The Graph data structure,that illustrates the 
abstract concept of a graph.*/
typedef struct
{
	void 	*data;
	int 	number_of_vertices;
	Vertex	*vertices;
} Graph;



/*All functions operate on the assertion that the given data structures are not NULL and that
the vertices are within the appropriate boundaries, if one of these assertions fails,the
execution terminates immediately.*/




/****************************************************************************************************************
The *graph_new() function,creates a new graph with specific number of vertices and zero initial edges.

	Parameters:
		int number_of_vertices, 	An integer that represents the number of vertices the graph can have.

	Return values:
		If all memory allocation was successful it returns a pointer to a Graph data structure.If no memory
		could be allocated then the execution terminates.
****************************************************************************************************************/
Graph 	*graph_new(int number_of_vertices);





/****************************************************************************************************************
The graph_add_edge() function,adds a new edge to a vertex as well as some data related to that edge.

	Parameters:
		Graph *g, 		A pointer to a Graph data structure.
		Label v, 		The start vertex of the edge.
		Label u,		The end vertex of the edge.
		void *data, 	Some data related to that edge.

	Return values:
			No return value,although if something goes wrong it prints a message to the user.
****************************************************************************************************************/
void	graph_add_edge(Graph *g,Label v,Label u,void *data);





/****************************************************************************************************************
The graph_del_edge() function, it doesn't actually delete an edge but rather sets it's data component 
to negative one, indicating deletion.

	Parameters:
		Graph *g, 		A pointer to a Graph data structure.
		Label v,		The start vertex of the edge to be deleted.
		Label u,		The end vertex of the edge to be deleted.

	Return values:
		No return value.If an edge has many duplicates it only deletes one.
****************************************************************************************************************/
void 	graph_del_edge(Graph *g,Label v,Label u);





/****************************************************************************************************************
The graph_has_edge() function, checks if an edge exists in the graph.

	Parameters:
		Graph *g, 		A pointer to a Graph data structure.
		Label v,		The start vertex of the edge to be deleted.
		Label u,		The end vertex of the edge to be deleted.

	Return values:
		If the edge exists it returns one, otherwise it returns zero.
****************************************************************************************************************/
int 	graph_has_edge(Graph *g,Label v,Label u);





/****************************************************************************************************************
The *graph_get_edge_array() function,returns the edges,a specific vertex has.

	Parameters:
		Graph *g,			A pointer to a Graph data structure.
		Label v,			The label of the vertex.
		int *num_edges,		A pointer to an integer,which will contain the number of edges the vertex has.

	Return values:
		Returns a pointer to an array of Edge data structure.
****************************************************************************************************************/
Edge 	*graph_get_edge_array(Graph *g,Label v,int *num_edges);






/****************************************************************************************************************
The graph_set_vertex_data() function, adds or updates the existing data component of some vertex.

	Parameters:
		Graph *g,			A pointer to a Graph data structure.
		Label v,			The label of the vertex.
		void *data,			The new value of the data component.

	Return values:
		Returns nothing.Although if the graph is non-existent, it prints a message and terminates.
****************************************************************************************************************/
void 	graph_set_vertex_data(Graph *g,Label v,void *data);






/*Hasn't been implemented.*/
void 	graph_default_explore(Graph *g,Label v);





/****************************************************************************************************************
The graph_dfs() higher order function, performs a depth first search in a graph.

	Parameters:
		Graph *g, 								A pointer to a Graph data structure.
		Label v,								A start vertex.
		void (explore)(Graph *g,Label v)		A pointer to a function that explores the graph.

	Return values:
		Returns nothing,although if the graph doesn't exist or the vertices are out of range it
		prints and prints a message and terminates.	
****************************************************************************************************************/
void 	graph_dfs(Graph *g,Label v,void (explore)(Graph *g,Label v));





/****************************************************************************************************************
The graph_destroy() function, deallocates all memory that's associated with the given Graph data structure.

	Parameters:
		Graph *g,	A pointer to a Graph data structure.

	Return values:
		Returns nothing.
****************************************************************************************************************/
void 	graph_destroy(Graph *g);
