/*****************************************************
*
*
*	Author: Endri Kastrati
*
*	Heap data structure and function prototyping.
*	Written for assignment2 of COMP20007.
*
*
******************************************************/


/*Constant definition of boolean values.*/
#define HEAP_SUCCESS 	1
#define HEAP_FAIL 		0	



/*A shortcut definition for unsigned integer.*/
typedef unsigned int uint;



/*The HeapItem data structure has two components,
the key for deciding position in the heap and
the payload index provided by the calling program.*/
typedef struct item
{
	float 	key;
	uint 	dataIndex;
} HeapItem;



/*The Heap data structure which consists of an
array of HeapItem structs,map[i] is index into H
of location of payload with dataIndex==i,the
current size of the heap and the total allocated size.*/
typedef struct heap
{
	HeapItem 	*H;
	uint 		*map;
	uint 		n;
	uint 		size;
} Heap;




/*All functions operate on the assertion that the given data structures are not NULL and that
the variables of uint type are greater or equal to zero, if one of these assertions fails,the
execution terminates immediately.*/



/****************************************************************************************************************
The *createHeap() function, creates a new,empty Heap data structure (allocating memory for components and etc..)
and returns a pointer to it.

	Parameters: 	No parameters.

	Return values:
		Returns a pointer to a Heap data structure.
****************************************************************************************************************/
Heap 	*createHeap();





/****************************************************************************************************************
The insert() function, inserts into the Heap an item without distorting the heap property.

	Parameters:
		Heap *h, 			A pointer to a Heap data structure.
		uint dataIndex,		The payload index provided by the program.
		float key, 			The key value.

	Return values:
		It returns either one for successful insertion or zero for failure.
****************************************************************************************************************/
int 	insert(Heap *h,uint dataIndex,float key);




/****************************************************************************************************************
The peek() function, returns the dataIndex of the root element in the heap tree.

	Parameters:
		Heap *h, 		A pointer to a Heap data structure.

	Return values:
		an unsigned integer which is the dataIndex of the root.
****************************************************************************************************************/
uint 	peek(Heap *h);





/****************************************************************************************************************
The peekKey() function, returns the key value of the root element in the heap tree.
	
	Parameters:
		Heap *h, 		A pointer to a Heap data structure.

	Return values:
		a float which is the key value of the root.
****************************************************************************************************************/
float 	peekKey(Heap *h);





/****************************************************************************************************************
The removeMin() function, removes the root element from the heap and re-heapifies restoring the heap property
throughout the tree.

	Parameters:
		Heap *h,		A pointer to a Heap data structure.

	Return values:
		Returns the dataIndex to the poped-out element.
****************************************************************************************************************/
uint 	removeMin(Heap *h);



/****************************************************************************************************************
The changeKey() function, changes the key value of a specific element in the heap and depending on the new value
either sifts up or down thus preserving the heap property.

	Parameters:
		Heap *h,			A pointer to a Heap data structure.
		uint dataIndex,		the payload index.
		float delta,		The new key value.

	Return values:
		returns nothing.
****************************************************************************************************************/
void 	changeKey(Heap *h,uint dataIndex,float delta);





/****************************************************************************************************************
The destroyHeap() function, deallocates memory for the Heap data structure and all it's components.

	Parameters:
		Heap *h,		A pointer to a Heap data structure.

	Return values:
		returns nothing.
****************************************************************************************************************/
void 	destroyHeap(Heap *h);
