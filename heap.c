/*****************************************************
*
*
*	Author: Endri Kastrati
*
*	Function defintion for heap operations.
*	Written for assignment2 of COMP20007.
*
*
******************************************************/


/*Including the standard input-output library.*/
#include <stdio.h>

/*Including the standard general utilities library.*/
#include <stdlib.h>

/*Including the standard assertion library.*/
#include <assert.h>

/*Including the heap library.*/
#include "heap.h"

/*Including the general string manipulation library.*/
#include <string.h>



/*A private function for float comparison,
returns the difference of the two given
float parameters *a-*b.*/
static float cmp(float *a,float *b)
{
	return *a-*b;
}



/*A private function for implementing iterative 
sift up procedure for a Heap data structure.*/
static void sift_up(Heap *h,uint k)
{
	/*Variable declarations and assertions.*/
	assert(h!=NULL);
	HeapItem temp;

	while (k>0 && cmp(&h->H[k/2].key,&h->H[k].key)>=0)
	{
		temp=h->H[k];
		h->map[h->H[k].dataIndex]=k/2;
		h->H[k]=h->H[k/2];
		h->map[h->H[k/2].dataIndex]=k;
		h->H[k/2]=temp;
		k=k/2;
	}

	return;
}



/*A private function for implementing iterative
sift down procedure for a Heap data structure.*/
static void sift_down(Heap *h,uint k,uint n)
{
	/*Variable declarations,initialisations
	and assertions.*/
	assert(h!=NULL);
	assert(k>=0 && n>=0);
	uint j;
	HeapItem temp;
	uint all_equal_end_loop=0;

	while (2*k+1<n)
	{
		j=2*k;

		if (j<=n && cmp(&h->H[j].key,&h->H[j+1].key)>=0)
		{
			j++;
		}

		temp=h->H[k];
		h->map[h->H[k].dataIndex]=j;
		h->H[k]=h->H[j];
		h->map[h->H[j].dataIndex]=k;
		h->H[j]=temp;
		k=j;

		if (all_equal_end_loop>=n)
		{
			return;
		}

		all_equal_end_loop++;
	}
}



/*This function creates a new Heap
data structure and returns a pointer to
that new struct.*/
Heap *createHeap()
{
	/*Variable declarations and initialisations.*/
	uint initial_heap_size=1;
	Heap *heap=NULL;

	/*Allocate memory for the Heap data structure
	and all of it's components.*/

	heap=(Heap *)malloc(sizeof(*heap));
	assert(heap!=NULL);
	heap->H=(HeapItem *)malloc(initial_heap_size*sizeof(HeapItem));
	assert(heap->H!=NULL);
	heap->map=(uint *)malloc(initial_heap_size*sizeof(uint));

	/*Set last element index to zero,
	initial size to one and return pointer
	to new Heap struct.*/

	heap->n=0;
	heap->size=initial_heap_size;
	return heap;
}



/*This function inserts a new item into the last position in
the heap and sifts up so heap property is preserved.*/
int insert(Heap *h,uint dataIndex,float key)
{
	/*Variable declarations and assertions.*/
	uint double_size;
	assert(h!=NULL);
	assert(dataIndex>=0);

	/*Check if there is enough space,if not
	reallocate more memory.*/

	if (h->n+1==h->size)
	{
		double_size=2*h->size;
		h->H=(HeapItem *)realloc(h->H,double_size*sizeof(HeapItem));
		assert(h->H!=NULL);
		h->map=(uint *)realloc(h->map,double_size*sizeof(uint));
		assert(h->map!=NULL);
		h->size=double_size;
	}

	/*Add item in the last position,
	sift up and increment the last index
	of the heap array.*/

	h->H[h->n].key=key;
	h->H[h->n].dataIndex=dataIndex;
	h->map[dataIndex]=h->n;
	sift_up(h,h->n);
	h->n++;

	return HEAP_SUCCESS;		
}



/*This function changes the key value of a heap item,
and depending on the change it either sifts down
or sifts up.*/
void changeKey(Heap *h,uint dataIndex,float delta)
{
	/*Variable declarations and assertions.*/
	assert(h!=NULL);
	assert(dataIndex>=0);
	assert(h->map[dataIndex]>=0);
	float temp_previous;

	/*Remember the previous key value by storing it
	into a variable.Overwrite previous key value of
	heap item with new one.If new key value is greater 
	or equal to previous sift down,otherwise sift up.*/

	temp_previous=h->H[h->map[dataIndex]].key;
	h->H[h->map[dataIndex]].key=delta;

	if (temp_previous<=h->H[h->map[dataIndex]].key)
	{
		sift_down(h,h->map[dataIndex],h->n);
	}
	else
	{
		sift_up(h,h->map[dataIndex]);
	}
}



/*This function removes the root
element of the min-heap tree,re-heapifies
and returns the id of the removed element.*/
uint removeMin(Heap *h)
{
	/*Variable declarations and assertions.*/
	assert(h!=NULL);
	HeapItem temp;
	uint root_id,i;

	/*Before removing the root,assign
	it's id value to the root_id variable.Swap the root with the last element
	in the heap and decrement size of heap by one.Once done, reheapify
	so that the heap property is preserved throughout the entire tree.*/

	root_id=h->H[0].dataIndex;
	h->map[root_id]=-1;
	temp=h->H[0];
	h->H[0]=h->H[h->n-1];
	h->H[h->n-1]=temp;
	h->n--;
	sift_down(h,0,h->n);

	return root_id;
}



/*This function returns
the id of the root element
int the heap.*/
uint peek(Heap *h)
{
	assert(h!=NULL);
	return h->H[0].dataIndex;
}



/*This function returns the key value
of the root element in the heap.*/
float peekKey(Heap *h)
{
	assert(h!=NULL);
	return h->H[0].key;
}



/*This function deallocates memory
for a Heap data structure and it's 
components.*/
void destroyHeap(Heap *h)
{
	assert(h!=NULL);
	free(h->H);
	free(h->map);
	free(h);
	return;
}


