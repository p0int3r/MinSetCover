/*****************************************************
*
*
*	Author: Endri Kastrati
*
*	Main execution program for Assignment2.
*	Written for assignment2 of COMP20007.
*
*
******************************************************/


/*Including the standard input-output library.*/
#include <stdio.h>

/*Including the standard general utilities library.*/
#include <stdlib.h>

/*Including the standard assertion library.*/
#include <assert.h>

/*Including graph ass2 library.*/
#include "ass2.h"


/*The main execution program for minimum set cover
problem of assignment2 for COMP20007 using a greedy algorithm.*/
int main(int argc,char *argv[])
{

	/*Variable Declarations and initializations.*/
	Graph *g;
	uint schools,houses,limit=1000,i,j;
	Sets *sets;
	Set *X,*results;

	/*Read values from input file and create a Graph data structure,
	once the execution of read_connected_graph() has been completed
	create a Sets data structure based on the number of school sites.*/

	g=read_connected_graph(NULL,&houses,&schools);
	sets=create_sets_array(schools);


	/*Run dijkstra's algorithm to return all reachable towns
	within 2km from every school site.*/

	for (i=0;i<sets->size;i++)
	{
		sets->S[i]=run_dijkstra(g,houses+i,limit,houses);
		sets->map[i]=houses+i;
	}


	/*Create a set X and insert unique vertices from each set
	such that X contains all reachable houses.*/

	X=create_set();

	for (i=0;i<sets->size;i++)
	{
		X=join_sets(X,sets->S[i],houses);
	}

	/*Calculate the minimum number of schools that needs to be build
	such that no child has to travel more than 1km.
	Print the results to the user.*/

	results=greedy_set_cover(sets,X,houses);
	print_results(results,houses);

	/*Free all memory allocated for the
	Graph,Sets and Set data structures.*/

	destroy_set(X);
	graph_destroy(g);
	destroy_set(results);
	destroy_sets_array(sets);

	return 0;	/*If successful return zero and control to the operating system.*/
}
