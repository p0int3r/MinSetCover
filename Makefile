OBJ     = graph.o heap.o set.o ass2.o main.o
SRC     = graph.c heap.c set.c ass2.c main.c
EXE     = setCover

CC    = g++
CDEFS = -u -Wall -O2

set:   $(OBJ) Makefile
	$(CC)    $(CDEFS) -o $(EXE) $(OBJ)

clean:
	rm -f $(OBJ) $(EXE)

clobber: clean
	rm -f $(EXE)


